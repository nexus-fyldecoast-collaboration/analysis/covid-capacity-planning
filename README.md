# covid-capacity-planning

Planning for COVID response, based upon Imperial College assumptions/analysis, and the Fylde Coast demographics.

## How to Run
Load the project ```bth_coronavirus.Rproj``` into R, and run ```bth_coronavirus.R```.
This will launch the shiny app in-browser, allowing the user to select their parameters, run models, and save output data into Excel workbooks.

The model takes the latest figures as input, and estimates future projections
based upon these. Data can be loaded in in the function: ```get_covid_blackpool()```.
Currently this function pulls data from either [PHE's daily data](https://fingertips.phe.org.uk/documents/Historic%20COVID-19%20Dashboard%20Data.xlsx),
or from saved CSVs containing daily data. If the data is loaded from CSVs, then
the function may need modifying to fit the data put in. The input data should
be modified so that when called, it is in a dataframe containing cumulative sums
of cases (estimated), hospitalisations, critical-care cases (e.g. ITU), patients
in the mortuary, recovered patients, and deceased patients. These figures should
be aggregated and ordered by date and 10-year age-band.

## The Model
An extended age-structured compartmental model is used to roughly forecast the
expected numbers of COVID-19 cases. Eight compartments are used to model the
passage of the virus through the population:

* $`P`$ = Susceptible population
* $`I`$ = Asymptomatic infected population
* $`S`$ = Symptomatic non-hospitalised infected population
* $`H`$ = Hospitalised non-critical infected population
* $`C`$ = Critical-Care infected population
* $`M`$ = Dead population in mortuary
* $`D`$ = Dead population not in mortuary
* $`R`$ = Recovered population

The transformation of all components are updated via the Ordinary Differential
Equations:

```math
\frac{dP}{dt} = -\frac{\beta I_{tot} P}{N},
```
```math
\frac{dI}{dt} = \frac{\beta I_{tot} P}{N} - (1 - \alpha_{s}) \gamma_{r} I - \alpha_{s} \sigma_{s} I,
```
```math
\frac{dS}{dt} = \alpha_{s} \sigma_{s} I - (1 - \alpha_{h}) \big((1 - \alpha_{d}) \gamma_{r} + \alpha_{d} \gamma_{d} \big) S - \alpha_{h} \sigma_{h} S,
```
```math
\frac{dH}{dt} = \alpha_{h} \sigma_{h} S - (1 - \alpha_{c}) \big((1 - \alpha_{d}) \gamma_{r} + \alpha_{d} \gamma_{d} \big) H - \alpha_{c} \sigma_{c} H,
```
```math
\frac{dC}{dt} = \alpha_{c} \sigma_{c} H - \big((1 - \alpha_{d}) \gamma_{r} + \alpha_{d} \gamma_{d} \big) C,
```
```math
\frac{dM}{dt} = \alpha_{d} \gamma_{d} \big(S (1 - \alpha_{h}) + H (1 - \alpha_{c}) + C\big) - \sigma_{m} M,
```
```math
\frac{dD}{dt} = \sigma_{m} M,
```
```math
\frac{dR}{dt} =  \gamma_{r} I (1 - \alpha_{s}) + \gamma_{r} \big(1 - \alpha_{d}) (S (1 - \alpha_{h}) + H (1 - \alpha_{c}) + C\big).
```

Here, $`N = P + I + S + H + C + M + D + R`$ is the total population,
$`I_{tot} = S + \frac{I}{2}`$ is the infectious population (assuming asymptomatic people are half as infectious as symptomatic),
$`\gamma_{...}`$ is the inverse
time (in days) to an event (e.g. if recovery takes 7 days, then
$`\gamma_{recover} = 1 / 7`$), and $`\sigma_{x}`$ is the inverse time (in days)
to event $`x`$, where $`x`$ is one of ($`s`$ = symptoms appearing, $`h`$ = hospitalisation, $`c`$ = critical care requirement, $`m`$ = mortuary exit).
The percentage of the population which enter state $`x`$ is given by $`\alpha_x`$, where
$`x`$ is one of ($`s`$ = symptomatic, $`h`$ = hospitalised, $`c`$ = critical care required, $`d`$ = dead, $`r`$ = recovered).
The spread of the virus is controlled via $`\beta`$, which is calculated as
$`\beta = R(t) (\gamma_r + \gamma_d) / 2`$.

The ODEs above are solved via a fourth-order Runge-Kutta scheme, and the outputs
are displayed within the shiny app for the user to view.

Confidence intervals are calculated through Monte Carlo simulations. The virus
reproduction number ($`R(t)`$) is drawn n-times from a Gamma distribution, roughly
matching that calculated by [Flaxman et al. (2020)](https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-Europe-estimates-and-NPI-impact-30-03-2020.pdf).

## Loading Custom Data
For information on how to load run the model with custom datasets (i.e. demographic/COVID testing figures outwith the Fylde Coast region), please refer to the [User Guide](UserGuide.pdf).

## License
Please reference the NHS Fylde Coast CCGs (Digital Intelligence Unit) if you wish to re-use work presented here.
