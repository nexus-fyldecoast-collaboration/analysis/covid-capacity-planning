select_ui <- function(app_type = 'input') {
  if (app_type == 'input') {
    # Define UI for dataset viewer app ----
    ui <- fluidPage(
      titlePanel("COVID-19 Estimates"),

      # Sidebar layout with a input and output definitions ----
      sidebarLayout(
        # Sidebar panel for inputs ----
        sidebarPanel(
          # Input: Selector for choosing dataset ----
          selectInput(inputId = "r0",
                      label = "Choose an initial R0:",
                      choices = seq(0.0, 2.6, 0.1),
                      selected = 2.4),

          # Input: Selector for choosing first case (or first death really) ----
          dateInput(inputId = "first_case",
                    label = "When was the first confirmed case in Blackpool?",
                    value = ymd('2020-02-01')),
          dateInput(inputId = "first_intervention",
                    label = "When was the first intervention put in place?",
                    value = ymd('2020-03-24')),
          dateInput(inputId = "end_first_intervention",
                    label = "When does the first intervention end?",
                    value = ymd('2021-03-01')),
          selectInput(inputId = "r0_1",
                      label = "Choose an R0 for the first intervention:",
                      choices = seq(0.0, 2.6, 0.1),
                      selected = 1.6),
          numericInput(inputId = "implementation_days",
                       label = "How many days does it take for an intervention to be fully put in place?",
                       value = 10)
        ),

        # Main panel for displaying outputs ----
        mainPanel(
          plotOutput('r0_plot'),
          plotOutput('sir_plot'),
          plotOutput('beds_plot')
        )
      )
    )
  } else {
    # Define UI for dataset viewer app ----
    ui <- fluidPage(
      titlePanel("COVID-19 Estimates"),

      # Sidebar layout with a input and output definitions ----
      sidebarLayout(
        # Sidebar panel for inputs ----
        sidebarPanel(
          numericInput(inputId = "dt",
                       label = "Choose a simulation time-step [days] (Setting lower will increase accuracy but slow down models)",
                       value = 1),
          numericInput(inputId = "n",
                       label = "Choose number of Monte Carlo simulations (Setting higher will increase accuracy but slow down models)",
                       value = 1e3),
          numericInput(inputId = "ci",
                       label = "Select confidence level",
                       value = 0.95),
          numericInput(inputId = "days_to_symptom",
                       label = "Select average days from infected to symptomatic",
                       value = 5),
          numericInput(inputId = "days_to_hospital",
                       label = "Select average days from symptomatic to hospitalised",
                       value = 7),
          numericInput(inputId = "days_to_critical",
                       label = "Select average days from hospitalised to ICU",
                       value = 2),
          numericInput(inputId = "days_to_recovered",
                       label = "Select average days from infected to recovered",
                       value = 22),
          numericInput(inputId = "days_to_death",
                       label = "Select average days from infected to dead",
                       value = 17),
          numericInput(inputId = "days_in_mortuary",
                       label = "Select minimum days in mortuary",
                       value = 9),
          checkboxInput(inputId = "optimise_mortuary",
                       label = "Re-Estimate Infection Fatality Ratio based upon loaded data? (Warning, requires a significant amount of time to optimise parameters)",
                       value = FALSE),
          actionButton("button", "Run Model"),
          downloadButton("download_data", "Download Model Output")
        ),

        # Main panel for displaying outputs ----
        mainPanel(
          p(
            strong('Assumed reproductive number, with 95% confidence interval.'),
            align = 'center'
          ),
          plotOutput('r0_plot') %>% withSpinner(color="#0dc5c1", type = 7),
          p(
            strong(
              'Number of infections, showing real hospitalisations (black dots)
              estimated cases from hospitalisation (red dots) and forecast with
              confidence interval (blue shaded).'
            ),
            align = 'center'
          ),
          plotOutput('infected_real') %>% withSpinner(color="#0dc5c1", type = 7),
          # p(
          #   strong(
          #     'Number of new daily hospitalisations (blue), critical cases (red),
          #     and deaths (black) for the forecast with confidence interval (shaded).'
          #   ),
          #   align = 'center'
          # ),
          # plotOutput('sir_plot') %>% withSpinner(color="#0dc5c1", type = 7),
          p(
            strong(
              'Forecast usage of hospital beds (blue) and ICU beds (red)
              with confidence interval (shaded).'
            ),
            align = 'center'
          ),
          plotOutput('beds_plot') %>% withSpinner(color="#0dc5c1", type = 7),
          p(
            strong('Forecast mortuary usage with confidence interval (shaded).'),
            align = 'center'
          ),
          plotOutput('mortuary_plot') %>% withSpinner(color="#0dc5c1", type = 7),
          p(strong('Forecast hospital bed usage by age-band.'), align = 'center'),
          plotOutput('beds_plot_by_age') %>% withSpinner(color="#0dc5c1", type = 7),
          p(strong('Forecast ICU bed usage by age-band.'), align = 'center'),
          plotOutput('icu_plot_by_age') %>% withSpinner(color="#0dc5c1", type = 7),
          p(strong('Forecast mortuary usage by age-band.'), align = 'center'),
          plotOutput('death_plot_by_age') %>% withSpinner(color="#0dc5c1", type = 7)
        )
      )
    )
  }

  ui
}
