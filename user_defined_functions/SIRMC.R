# Create a SIR function
# sir_mc <- function(time, state, parameters) {
#   n_compartments <- 7
#   n_ages <- length(state) / n_compartments
#   S <- as.matrix(state[1 : n_ages])
#   I <- as.matrix(state[(n_ages + 1) : (2 * n_ages)])
#   H <- as.matrix(state[(2 * n_ages + 1) : (3 * n_ages)])
#   CC <- as.matrix(state[(3 * n_ages + 1) : (4 * n_ages)])
#   R <- as.matrix(state[(4 * n_ages + 1) : (5 * n_ages)])
#   D <- as.matrix(state[(5 * n_ages + 1) : (6 * n_ages)])
#   
#   with(as.list(c(parameters)), {
#     N <- S + I + R + D
#     beta <- beta[dt * time + 1]
#     
#     SI <- beta * S * I / N
#     dD <- gamma_d * I * percent_die
#     # dD <- (gamma_r + percent_die) * I
#     dR <- gamma_r * I * (1 - percent_die)
#     # dR <- (gamma_r - percent_die) * I
#     
#     IR <- dD + dR
#     
#     dS <- -SI
#     dI <- SI - IR
#     dSymptom <- (SI - IR) * percent_symptomatic
#     dH <- (SI - IR) * percent_hospitalised
#     dCC <- (SI - IR) * percent_cc
#     return(list(c(dS, dI, dSymptom, dH, dCC, dD, dR)))
#   })
# }

sir_mc <- function(time, state, parameters) {
  n_compartments <- 7
  n_ages <- length(state) / n_compartments
  S <- as.matrix(state[1 : n_ages])
  I <- as.matrix(state[(n_ages + 1) : (2 * n_ages)])
  H <- as.matrix(state[(2 * n_ages + 1) : (3 * n_ages)])
  CC <- as.matrix(state[(3 * n_ages + 1) : (4 * n_ages)])
  R <- as.matrix(state[(4 * n_ages + 1) : (5 * n_ages)])
  D <- as.matrix(state[(5 * n_ages + 1) : (6 * n_ages)])
  
  with(as.list(c(parameters)), {
    N <- S + I + R + D
    beta <- beta
    
    SI <- beta * S * I / N
    dD <- gamma_d * I * percent_die
    dR <- gamma_r * I * (1 - percent_die)
    
    IR <- dD + dR
    
    dS <- -SI
    dI <- SI - IR
    dSymptom <- (SI - IR) * percent_symptomatic
    dH <- (SI - IR) * percent_hospitalised
    dCC <- (SI - IR) * percent_cc
    return(list(c(dS, dI, dSymptom, dH, dCC, dD, dR)))
  })
}

# Create a SIR function
seir_mc <- function(time, state, parameters) {
  with(as.list(c(state, parameters)), {
    N <- S + E + I + R + D
    beta <- beta[dt * time + 1]
    
    SE <- beta * S * I / N
    dD <- (gamma_d * I - mu * R) * percent_die
    dR <- (gamma_r * I - mu * R)* (1 - percent_die)
    
    IR <- ((gamma_d + mu) * percent_die + (gamma_d + mu) * (1 - percent_die))* I #dD + dR
    EI <- sigma * (E + mu)
    
    dS <- mu - mu * S - SE
    dE <- SE - EI
    dI <- EI - IR
    dSymptom <- (EI - IR) * percent_symptomatic
    dH <- (EI - IR) * percent_hospitalised
    dCC <- (EI - IR) * percent_cc
    return(list(c(dS, dE, dI, dSymptom, dH, dCC, dD, dR)))
  })
}

