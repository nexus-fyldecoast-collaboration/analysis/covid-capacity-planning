# Create a SIR function
sir_intervention <- function(time, state, parameters) {
  with(as.list(c(state, parameters)), {
    N <- S + I
    
    # Sinusoidal beta
    # beta <- beta0 - sin(4 * pi / 2 + n_reps * 2 * pi * time / max_time) * beta0 / 4
    
    r0_linear_a1 <- (r0 - r0_1) / implementation_days
    r0_linear_b1 <- first_intervention - implementation_days
    r0_linear_a2 <- (r0_1 - r0) / implementation_days
    r0_linear_b2 <- end_first_intervention - implementation_days
    
    # Not-Quite-Heaviside beta
    beta <- case_when(
      first_case + time <= first_intervention - implementation_days ~ 
        (gamma_r * r0),
      between(
        first_case + time, 
        first_intervention - implementation_days + 1e-5, 
        first_intervention - 1e-5
      ) ~ 
        gamma_r * (r0 - r0_linear_a1 * (first_case + time - r0_linear_b1)),
      between(
        first_case + time, 
        first_intervention, 
        end_first_intervention - implementation_days
      ) ~ 
        (gamma_r * r0_1),
      between(
        first_case + time, 
        end_first_intervention - implementation_days + 1e-5, 
        end_first_intervention - 1e-5
      ) ~ 
        gamma_r * (r0_1 - r0_linear_a2 * (first_case + time - r0_linear_b2)),
      first_case + time >= end_first_intervention ~ 
        (gamma_r * r0)
    )
    
    SI <- beta * S * I / N
    dD <- gamma_d * I * percent_die
    dR <- gamma_r * I * (1 - percent_die)
    
    IR <- dD + dR
    
    dS <- -SI
    dI <- SI - IR
    dSymptom <- (SI - IR) * percent_symptomatic
    dH <- (SI - IR) * percent_hospitalised
    dCC <- (SI - IR) * percent_cc
    return(list(c(dS, dI,dSymptom, dH, dCC, dD, dR)))
  })
}