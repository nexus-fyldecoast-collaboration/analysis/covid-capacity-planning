clean_data <- function(df) {
  names(df)[1] <- 'date'
  names(df)[2] <- 'count'
  
  df %>%
    transmute(
      date = as_date(ymd_hms(date)),
      Age10Cat = str_replace_all(Age10Cat, 'Oct-19', '10-19'),
      count = count
    )
}
