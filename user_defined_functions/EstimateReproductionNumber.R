estimate_reproduction_number <- function(
  base_data = 'confirmed_cases', plot_r = FALSE, estimation_method = 'SB', ...
) {
  
  # Choose whether to calculate R(t) from deaths or confirmed cases
  if (base_data == 'deaths') {
    
    # Assume infection fatality ratio of ~1%
    # Assume mean time from infection to death of 17 days
    ifr <- 0.01
    mean_death_days <- 17
    
    #read_csv('covid_19_deaths.csv')
    
    # Deaths
    uk <- get_covid_latest('deaths') %>%
      rename(
        country = Country.Region,
        area = Province.State
      ) %>%
      filter(country == 'United Kingdom' & is.na(area)) %>%
      select(-area, -Lat, -Long) %>%
      pivot_longer(-country, names_to = 'date', values_to = 'death_count') %>%
      mutate(
        date = mdy(date),
        case_estimate = lead(death_count / ifr, mean_death_days)
      )
  } else {
    
    # Confirmed cases
    uk <- get_covid_latest() %>%
      rename(
        country = Country.Region,
        area = Province.State
      ) %>%
      filter(country == 'United Kingdom' & is.na(area)) %>%
      dplyr::select(-area, -Lat, -Long) %>%
      pivot_longer(-country, names_to = 'date', values_to = 'confirmed_case') %>%
      mutate(
        date = mdy(date),
        case_estimate = 3 * confirmed_case
      )
  }
  
  # Generation time from https://academic.oup.com/jtm/article/27/2/taaa021/5735319
  covid_gt <- generation.time('gamma', c(8.4, 3.8))
  
  # # Generation time from https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-Europe-estimates-and-NPI-impact-30-03-2020.pdf
  # covid_gt <- generation.time('gamma', c(6.5, 0.65))
  
  cases <- uk %>%
    dplyr::select(date, case_estimate) %>%
    filter(!is.na(case_estimate))
  
  uk_cases <- cases$case_estimate
  names(uk_cases) <- cases$date
  uk_cases <- uk_cases[uk_cases > 0]
  
  cov_r <- estimate.R(
    uk_cases,
    GT = covid_gt,
    begin = 1L,
    end = length(uk_cases),
    methods = estimation_method,
    ...
  )
  
  if (plot_r) {
    plot(cov_r)
  }
  
  if (estimation_method == 'TD') {
    covid_rt <- data.frame(
      date = names(uk_cases),
      r = cov_r$estimates[[1]]$R,
      ci_lower = cov_r$estimates[[1]]$conf.int[, 1],
      ci_upper = cov_r$estimates[[1]]$conf.int[, 2]
    )
  } else {
    covid_rt <- data.frame(
      date = names(uk_cases),
      r = c(1, cov_r$estimates[[1]]$R),
      ci_lower = c(0, cov_r$estimates[[1]]$conf.int[, 1]),
      ci_upper = c(10, cov_r$estimates[[1]]$conf.int[, 2])
    )
  }
  
  covid_rt
}
