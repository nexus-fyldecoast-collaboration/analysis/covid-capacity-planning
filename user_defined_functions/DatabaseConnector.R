database_connector <- function(
  driver = config$sql_credentials$driver,
  server = config$sql_credentials$server,
  database = config$sql_credentials$database,
  port = config$sql_credentials$port,
  trusted_connection = TRUE
) {
  dbConnect(
    odbc(),
    Driver = driver,
    Server = server,
    Database = database,
    Port = port,
    Trusted_Connection = trusted_connection,
    timeout = 200
  )
}
