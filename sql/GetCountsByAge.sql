WITH bth_nel AS (
	SELECT [NHS Number], [Provider]
	FROM [AnalystGlobal].[RiskStratification].[PatientActivity]
	WHERE [Type] = 'IP_NEL'
),
prop_to_bth AS (
	SELECT 
		d.[lsoa],
		SUM(CASE WHEN a.[Provider] = 'BFWH (RXL)' THEN 1.0 ELSE 0.0 END) / COUNT(*) AS bth_nel_ratio
	FROM 
		[AnalystGlobal].[RiskStratification].[PatientDetail] d
	JOIN bth_nel a ON
		a.[NHS Number] = d.[NHS Number]
	GROUP BY
		[lsoa]
)
SELECT 
	FLOOR(
		(CASE WHEN d.[Age] > 80 THEN 80 ELSE d.[Age] END)
	/ 10) * 10 AS age_band_lower,
	SUM(bth_nel_ratio) AS expected_patients,
	COUNT(*) AS total_patients
FROM 
	[AnalystGlobal].[RiskStratification].[PatientDetail] d
JOIN prop_to_bth p ON
	p.[lsoa] = d.[lsoa]
GROUP BY
	FLOOR(
		(CASE WHEN [Age] > 80 THEN 80 ELSE [Age] END)
	/ 10) * 10
ORDER BY 
	FLOOR(
		(CASE WHEN [Age] > 80 THEN 80 ELSE [Age] END)
	/ 10) * 10