#.libPaths('C:/Users/alcockb1/Documents/R/win-library/3.6')
need_packages <- c(
  'config',
  'odbc',
  'lubridate',
  'readr',
  'openxlsx',
  'shiny',
  'deSolve',
  'R0', 
  'utils',
  'httr',
  'shinycssloaders',
  'tidyverse'
)

installed <- need_packages %in% installed.packages()
if(length(need_packages[!installed]) > 0) install.packages(need_packages[!installed])
lapply(need_packages, library, character.only = TRUE)
options(stringsAsFactors = FALSE)
options("openxlsx.dateFormat" = "dd/mm/yyyy")
config <- get()

# Load custom functions
invisible(
  lapply(
    Sys.glob("user_defined_functions/*.R"),
    function(x) source(x)
  )
)

ui <- select_ui('historic')
app <- shinyApp(ui = ui, server = run_shiny_mc_server)
runApp(app, launch.browser = TRUE)